/**
	roflcat
	Copyright (C) 2019 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CMD_HPP
#define CMD_HPP

#include <vector> //std::vector
#include <cstddef> //size_t


#define PRINT_LINE_NEVER    0
#define PRINT_LINE_ALWAYS   1
#define PRINT_LINE_NONBLANK 2

#define COLOR_DEFAULT       0
#define COLOR_FORCE         1
#define COLOR_DISABLE       2

//struct of command line options/values
struct cmd_args{
	std::vector<const char*> filenames;

	//lolcat options
	long seed = 0;
	float freq = 0.3f;
	float spread = 0.4f;
	unsigned truecol:1;
	unsigned color:2;
	unsigned invert:1;

	//GNU cat options
	unsigned number:2;
	unsigned ends:1;
	unsigned squeeze:1;
	unsigned tabs:1;
	unsigned nonprinting:1;
	unsigned treatbinary:1;

	//flags
	unsigned error:1;
	unsigned usage:1;
	unsigned version:1;

	cmd_args(void)noexcept;
	void clear_gnu_options(void);
	void set_nonprinting(bool = true);
};

struct cmd_options{
	const char* lopt;
	char sopt;
	const char* desc;
};

extern const cmd_options cmd_arguments_list[];
extern const size_t cmd_arguments_list_size;



cmd_args process_cmd_args(int argc, char** argv);

#endif
