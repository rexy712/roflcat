/**
	roflcat
	Copyright (C) 2019 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COLOR_PRINTER_BASE_HPP
#define COLOR_PRINTER_BASE_HPP

#include "printer_base.hpp"
#include "cmd.hpp"

//Provides some needed variables for the color printers
template<class Derived>
class color_printer_base : public printer_base<Derived>
{
protected:
	const wchar_t* const m_color_str;
	const float m_freq;
	const float m_spread;
	const bool m_invert;
protected:
	constexpr color_printer_base(const cmd_args& args, const wchar_t* col_str)noexcept:
		printer_base<Derived>(args.tabs ? SHOW_TABS_STRING : L"        ", args),
		m_color_str(col_str),
		m_freq(args.freq), m_spread(m_freq < 0 ? -args.spread : args.spread), m_invert(args.invert){}
	Derived& _reset(void){
		wprintf(L"\033[0m");
		return static_cast<Derived&>(*this);
	}
};

#endif
