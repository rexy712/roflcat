/**
	roflcat
	Copyright (C) 2019 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COLOR_PRINTER_MONO_HPP
#define COLOR_PRINTER_MONO_HPP

#include "printer_base.tpp"
#include "cmd.hpp"
#include <cstdio> //putwchar

//Uncolored printer
//Get it? color_printer_true and color_printer_false?!
class color_printer_false : public printer_base<color_printer_false>
{
friend class printer_base<color_printer_false>;
public:
	constexpr color_printer_false(void)noexcept = default;
	constexpr color_printer_false(const cmd_args& args)noexcept:
		printer_base(args.tabs ? SHOW_TABS_STRING : L"\t", args){}
protected:
	inline color_printer_false& _print(const wchar_t s)noexcept{
		putwchar(s);
		return *this;
	}
};
//don't worry, i gave a sensible alias name
using color_printer_mono = color_printer_false;

#endif
