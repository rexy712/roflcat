/**
	roflcat
	Copyright (C) 2019 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BASIC_COLOR_PRINTER_TPP
#define BASIC_COLOR_PRINTER_TPP

#include "basic_color_printer.hpp"

#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE //for wcwidth
#endif
#include <cwchar> //wcwidth

#include <cstdio> //wprintf, putwchar
#include <cstdlib> //rand

template<int I>
constexpr basic_color_printer<I>::basic_color_printer(const cmd_args& args)noexcept:
	color_printer_base<basic_color_printer<I>>(args, args.invert ? L"\033[38;5;0m\033[48;5;%sm" : L"\033[38;5;%sm"),
	m_randomness(rand()%color_lookup_len){}
template<int I>
basic_color_printer<I>& basic_color_printer<I>::_print(const wchar_t s)noexcept{
	if(s == L'\n'){
		inc_line();
		if(this->m_invert)
			this->reset();
		putwchar(L'\n');
		return *this;
	}else{
		int width = wcwidth(s);
		if(width > 0)
			inc_col(wcwidth(s));
		else
			inc_col(1);
	}
	long new_index = static_cast<int>(this->m_freq*m_col + this->m_spread*m_line + m_randomness) % color_lookup_len;
	if(new_index < 0)
		new_index += color_lookup_len;
	if(new_index != m_index){
		m_index = new_index;
		wprintf(this->m_color_str, color_lookup_table[m_index]);
	}
	putwchar(s);
	return *this;
}
template<int I>
constexpr basic_color_printer<I>& basic_color_printer<I>::_reset(void)noexcept{
	color_printer_base<basic_color_printer<I>>::_reset();
	m_index = -1; //force color update on next print
	return *this;
}
template<int I>
constexpr void basic_color_printer<I>::inc_col(int i){
	m_col += i;
}
template<int I>
constexpr void basic_color_printer<I>::inc_line(void){
	++m_line;
	m_col = 0;
}

#endif
