/**
	roflcat
	Copyright (C) 2019 rexy712

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BASIC_COLOR_PRINTER_HPP
#define BASIC_COLOR_PRINTER_HPP

#include "color_printer_base.hpp"

namespace bcp_detail{//lookup tables for the basic_color_printer class
	template<int I>
	struct lookup_table{};
	template<>
	struct lookup_table<8>{
		static constexpr const char* table[] = {"12","12","14","14","10","10","11","11","9","9","13","13"};
		static constexpr long len = sizeof(table)/sizeof(table[0]); //long because of arithmetic promotion during modulo
	};
	template<>
	struct lookup_table<16>:public lookup_table<8>{};

	template<>
	struct lookup_table<256>{
		static constexpr const char* table[] = {"39", "38", "44", "43", "49", "48", "84", "83", "119", "118", "154", "148", "184", "178", "214", "208", "209", "203", "204", "198", "199", "163", "164", "128", "129", "93", "99", "63", "69", "33"};
		static constexpr long len = sizeof(table)/sizeof(table[0]);
	};
}


//For color systems which have an easily enumerated amount of colors (256 and lower essentially)
template<int I>
class basic_color_printer : public color_printer_base<basic_color_printer<I>>
{
friend class printer_base<basic_color_printer<I>>;
private:
	static constexpr const char* const* color_lookup_table = bcp_detail::lookup_table<I>::table;
	static constexpr long color_lookup_len = bcp_detail::lookup_table<I>::len;
private:
	int m_col = 0;    //current column number
	int m_line = 0;   //current line number
	int m_index = -1; //index in lookup_table
	const int m_randomness; //change starting color
public:
	constexpr basic_color_printer(const cmd_args& args)noexcept;
	~basic_color_printer(void)noexcept = default;
protected:
	basic_color_printer& _print(const wchar_t s)noexcept;
	constexpr basic_color_printer& _reset(void)noexcept;
	constexpr void inc_col(int i);
	constexpr void inc_line(void);
};


#endif
