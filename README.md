# README

## About
roflcat is a C++ rewrite of [lolcat][1] which runs much faster and supports all the options of GNU cat. I am not going to support the animation options of lolcat (--animate, --duration, --speed) because there is no real purpose to them.  
Currently I can only confirm it runs on a linux system and I have no desire to expand to supporting anything else at this time.

To keep compatability with both GNU cat and lolcat, I wanted all command line options from both to either be implemented or ignored. However there are a few conflicts between the two, namely -s -t and -v. For these I will be implementing the GNU cat functionality.

Current version is 1.1 as of writing this, so if the version is much newer, remind me to update this readme :)  
![alt text](https://i.postimg.cc/0Qk6k34j/roflcat-usage.png "Screenshot of --help output")

## Dependencies
ncurses  
cmake

## Building
```
mkdir build
cd build
cmake ..
make
```

## Installing
From within the build directory:  
```
make install
```

## More Screenshots
Some test outputs on a file filled with laughing cat emojis

256 color:  
![alt text](https://i.postimg.cc/1tZC2k6r/roflcat-roflcats.png "roflcats in 256 color")

Truecolor (24 bit):  
![alt text](https://i.postimg.cc/tJF2527m/roflcat-roflcats-truecolor.png "roflcats in truecolor")

16 color (type that will work in a tty):  
![alt text](https://i.postimg.cc/LX99RbFj/roflcat-roflcats-16color.png "roflcats in 16 color")

[1]: https://github.com/busyloop/lolcat
